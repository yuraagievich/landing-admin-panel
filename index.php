<?php
session_start();
include_once('admin/adminData.php');
?>


    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/style.css?v=0002">
        <?php
        if ($_SESSION['al'] === 1) {
            echo " <script src='js/jquery-3.2.1.min.js'></script>";
            echo "<script src='tinymce/tinymce.min.js'></script>";
            echo "<link rel='stylesheet' href='css/styleAdmin.css'>";
            echo "<script src='js/adminScript.js?v=0002'></script>";
            echo "<section id='chengeOut'>
            <form action='admin/relog.php' method='POST'>
                <button name='relog' class='relog' type='submit' value='relog'>Сохранить и выйти</button>
            </form>
        </section>";
        }
        ?>




        <title>Children Smart Watch</title>
    </head>


    <div class="content">
        <body>

        <section class="head">
            <div class="container">
                <header>
                    <div class="logo">
                        <img src="img/logo.png" alt="Десткие Smart Watch">
                    </div>
                    <nav>
                        <ul>
                            <li><a href="#descr" class="changeArea descr"><?php echo file_get_contents('admin/variableFiles/descr.txt') ?></a></li>
                            <li><a href="#pay" class="changeArea pay"><?php echo file_get_contents('admin/variableFiles/pay.txt') ?></a></li>
                            <li><a href="#about" class="changeArea about"><?php echo file_get_contents('admin/variableFiles/about.txt') ?></a></li>
                            <li><a href="#reviews" class="changeArea reviews"><?php echo file_get_contents('admin/variableFiles/reviews.txt') ?></a></li>
                        </ul>
                        <span class="close_menu"></span>
                    </nav>
                    <div class="reverseBySize">
                        <div class="for_client">
                            <div class="phone">
                                <div class="changeArea phoneOne"><?php echo file_get_contents('admin/variableFiles/phoneOne.txt') ?></div>
                            </div>
                            <div class=" button order but_head"
                                 onclick="yaCounter50378515.reachGoal('targetButton1'); return true;"><span>Заказать звонок</span>
                            </div>
                        </div>
                        <div class="hamburger">
                            <span></span>
                        </div>
                    </div>

                </header>
            </div>
        </section>

        <section class="general">
            <div class="container">
                <!-- <h2>Хочешь узнать где твой ребенок сейчас?</h2>-->
                <div class="general_screen">
                    <div class="general_title_content">
                        <!-- <h1><span>Детские GPS часы</span> <span> Smart <span class="pink">K</span><span class="green">i</span><span class="blue">d</span><span class="yellow">s</span>  Watch</span></h1>-->
                        <h1>
                            <span>Хочешь знать</span>
                            <span> где твой</span>
                            <span> ребенок?</span>
                        </h1>

                    </div>
                </div>

                <div class="general_bunnton_and_price">
                    <span class="button but" onclick="yaCounter50378515.reachGoal('targetButton2'); return true;">Заказать</span>
                </div>
            </div>
        </section>

        <section class="about_watch" id="about">
            <div class="container">
                <div class="about_watch_container">
                    <h2>Что умеют часы Smart Kids Watch</h2>
                    <div class="about_descriptions">
                        <dl>
                            <dt>Маяк</dt>
                            <dd>Теперь вы постоянно в курсе, где Ваш ребенок. Благодаря встроенному GPS маячку вы можете
                                видеть
                                его
                                перемещение на карте.
                            </dd>

                            <dt>Гео "Забор"</dt>
                            <dd>Назначается виртуальная гео - зона на карте, диаметр (окружность) при выходе из которой,
                                часы
                                отправят
                                вам уведомление.
                            </dd>

                            <dt>Голосовые сообщения</dt>
                            <dd>Вы можете отправлять короткие голосовые сообщения на часы Вашего ребенка. Ребенок в свою
                                очередь
                                может
                                не только прослушать, но и ответить Вам.
                            </dd>

                            <dt>Разрешенные номера</dt>
                            <dd>Вы можете ввести 10 номеров, которые могут звонить Вашему ребенку. Звонки с телефонных
                                номеров,
                                не
                                внесенных в список не допускаются к дозвону.
                            </dd>

                            <dt>Здоровье</dt>
                            <dd>Время прогулки, кол-во шагов, кол-во затраченных каллорий, режим сна, полное ведение
                                журнала.
                            </dd>

                            <dt>Будильник</dt>
                            <dd>Возможность удаленно заводить будильник ребенка на часах.</dd>

                            <dt>Звонки с любых устройств</dt>
                            <dd>Часы Smart Kids Watch поддерживают звонки со стационарных и мобильных телефонов.</dd>
                        </dl>
                    </div>
                </div>
            </div>
        </section>

        <section class="photo_descr" id="descr">
            <div class="container">
                <h2>Smart Kids Watch Q528</h2>
                <div class="descr_container">
                    <div class="slider_photo">

                        <span class="leftSlidePhoto"></span>
                        <span class="rightSlidePhoto"></span>

                        <div class="sliderPhotoActiveArea">
                            <div class="sliderPhotoContent">

                                <div class="item">
                                    <img src="img/photoSlide3.jpg" alt="">
                                </div>

                                <div class="item">
                                    <img src="img/photoSlide1.jpg" alt="">
                                </div>

                                <div class="item">
                                    <img src="img/photoSlide2.jpg" alt="">
                                </div>


                            </div>
                        </div>

                    </div>
                    <div class="description_smart">
                        <div>
                            <h3>Описание</h3>
                            <span></span>
                            <p><span>Для кого: </span>Для детей от 4 до 11 лет</p>
                            <p><span>Дисплей: </span>Цветной сенсорный, 1,4"</p>
                            <p><span>Поддержка смартфонов: </span>Android, iOS</p>
                            <p><span>Сим-карта: </span>microSIM</p>
                            <p><span>Время работы: </span>от 24 до 72 часов (зависит от режима работы)</p>
                            <p><span>Габариты: </span>46 x 39 x 15 мм</p>
                            <p><span>Вес: </span>48 г.</p>
                            <p><span>Ремешок: </span>Гипоаллергенный силикон</p>
                            <p><span>Инструкция: </span>да</p>
                            <p><span>Гарантия: </span>365 дней</p>
                            <p class="all_price">Цена:
                                <span class="old_price">120,00 BYN</span>
                                <span class="new_price">98.80 BYN</span>
                            </p>
                            <span class="button" onclick="yaCounter50378515.reachGoal('targetButton3'); return true;">Заказать Smart Kids Watch</span>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="pay" class="pay_and_ship">
            <div class="container">
                <h2>Доставка и оплата</h2>
                <div class="payment_and_shipping">

                    <div class="working_time">
                        <p>
                            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="-3 -3 22 22"
                                 id="icon">
                                <path d="M11.927 11.46a6.003 6.003 0 0 0-2.225-1.536A4.5 4.5 0 1 0 3 6a4.5 4.5 0 0 0 2.298 3.923A6 6 0 0 0 1.5 15.5v1h1v-1a5 5 0 0 1 5-5c1.492 0 2.816.668 3.732 1.703.198-.28.435-.52.695-.742zM4 6a3.5 3.5 0 0 1 7 0 3.5 3.5 0 0 1-7 0zm12.357 6.61a.485.485 0 0 0-.684 0l-2.695 2.7-.684-.684a.482.482 0 0 0-.683.684l.935.936c.02.04.04.078.072.11.1.1.23.142.36.138.13.004.26-.04.36-.14.03-.03.05-.07.07-.108l2.948-2.95c.19-.19.19-.497.002-.687z"></path>
                            </svg>
                        </p>
                        <p>Время работы:</p>
                        <p>Мы работаем ежедневно для <span>Вас</span></p>
                        <p>с 09:00 до 21:00 </p>
                    </div>
                    <div class="free_shipping">
                        <p>
                            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="-3 -3 22 22"
                                 id="icon">
                                <path d="M16 5.493a.5.5 0 0 0-.5-.5H12v-.995A.998.998 0 0 0 11 3H2a1 1 0 0 0-1 .998v6.99a1 1 0 0 0 1 .997h.054a2.5 2.5 0 1 0 4.892 0h3.11a2.5 2.5 0 1 0 4.89 0H16.5a.5.5 0 0 0 .5-.497l-1-5.993zM12 6h3l.402 2.008H12V6zm-7.5 7.988c-.83 0-1.5-.666-1.5-1.487 0-.82.67-1.487 1.5-1.487.828 0 1.5.666 1.5 1.488s-.672 1.488-1.5 1.488zm6.01-2.986l-.01.002H6.49A2.49 2.49 0 0 0 4.5 10c-.817 0-1.535.398-1.99 1.004H2.5a.5.5 0 0 1-.5-.5V4.512a.5.5 0 0 1 .5-.498h8a.5.5 0 0 1 .5.5v5.993l-.002.01a2.48 2.48 0 0 0-.488.487zm1.99 2.986c-.828 0-1.5-.666-1.5-1.487 0-.82.672-1.487 1.5-1.487S14 11.68 14 12.5s-.672 1.488-1.5 1.488zM14.488 11A2.488 2.488 0 0 0 12 10.052V8.99h3.598L16 11h-1.512z">
                                </path>
                            </svg>
                        </p>
                        <p>Бесплатная доставка</p>
                        <p>Доставка по Минску -<span>БЕСПЛАТНО!</span></p>
                        <p>(в день заказа)</p>
                        <p>Доставка по Беларуси - 8 рублей</p>
                        <div>
                            <p>Чтобы оформить заказ, позвоните нам или </p>
                            <p>оставьте заявку, и мы с вами свяжемся.</p>
                        </div>

                    </div>
                    <div class="payment_methods">
                        <p>
                            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="-3 -3 22 22"
                                 id="icon">
                                <path d="M14.5 4V1a1 1 0 0 0-1-1l-12 4a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h13a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1zM13 1a.5.5 0 0 1 .5.5V4H4.75L13 1zm1.5 10h-2v-1h2v1zm0-2h-3v3h3v2.5a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-9A.5.5 0 0 1 2 5h12a.5.5 0 0 1 .5.5V9z"></path>
                            </svg>
                        </p>
                        <p>Способы оплаты</p>
                        <p>Оплата возможна: наличными или банковской </p>
                        <p>картой, <span>только при получении Вами часов</span>.</p>
                    </div>

                </div>
            </div>
        </section>


        <section class="reviews" id="reviews">
            <div class="container">
                <div class="reviews_container">
                    <h2>Отзывы</h2>
                    <div class="review_cont">
                        <div class="review">
                            <div class="reviews_images">
                                <img src="img/user8_8.jpg" alt="">
                            </div>
                            <div class="reviews_descriptions">
                                <p>Выражаю огромное СПАСИБО за смарт-часы и отдельное СПАСИБО службе технической
                                    поддержки. Я
                                    как мама, захотела подключить несколько приложений отслеживания и как результат часы
                                    нигде
                                    не подключались(в инструкции было написано ОДИН раз регистрировать, но я не верила).
                                    Если бы
                                    не техподдержка то часы свою основную функцию не выполняли бы. Спасибо!!!!</p>
                            </div>
                        </div>
                        <div class="review">
                            <div class="reviews_images">
                                <img src="img/user7_7.jpg" alt="">
                            </div>
                            <div class="reviews_descriptions">
                                <p>Спасибо! Отличная компания, всё чётко-гладко, толково-доходчиво и самое главное
                                    доброжелательно и позитивно. Заказ (детские умные часы) привезли в оговорённое
                                    время, очень
                                    доброжелательно разъяснили-объяснили что к чему, всему обучили, оставили контактный
                                    телефон,
                                    помогли с настройками. Всё супер! Спасибо всей команде.</p>

                            </div>
                        </div>
                        <div class="review">
                            <div class="reviews_images">
                                <img src="img/user9_9.jpg" alt="">
                            </div>
                            <div class="reviews_descriptions">
                                <p>Все просто замечательно! Покупал смарт-часы своему сыну!) Качество самих часов просто
                                    на
                                    высоте! Связались со мной в течении минут 15! Менеджер Михаил,все чётко,вежливо и
                                    понятно
                                    мне разъяснил!) Привезли в течении двух часов !! Вообщем я очень и очень доволен!
                                    Ребята
                                    действительно ставят акцент на качество и обслуживание!! Еще обращусь к вам и не
                                    раз!
                                    Спасибо вам большое!!</p>
                            </div>
                        </div>
                        <div class="review">
                            <div class="reviews_images">
                                <img src="img/user6_6.jpg" alt="">
                            </div>
                            <div class="reviews_descriptions">
                                <p>Отличная компания! Товар полностью соответствует указанному на сайте, как цена, так и
                                    характеристики. Доставка точно в срок, помощь при подключении. Дочка очень довольна.
                                    Рекомендую!</p>
                            </div>
                        </div>
                        <div class="review">
                            <div class="reviews_images">
                                <img src="img/user5_5.jpg" alt="">
                            </div>
                            <div class="reviews_descriptions">
                                <p>Магазин сработал отлично, товар был в наличии и по указанной цене, доставили на
                                    следующий
                                    день после заказа по Беларуси.</p>
                            </div>
                        </div>
                        <div class="review">
                            <div class="reviews_images">
                                <img src="img/user4_4.jpg" alt="">
                            </div>
                            <div class="reviews_descriptions">
                                <p>Покупали вместе с женой ребёнку в подарок умные часы. Ребята рассказали человеческим
                                    языком о
                                    функциях модели и быстро подобрали цвет. Качеством товара и сервисом остались очень
                                    довольны. Спасибо.</p>
                            </div>


                        </div>
                        <div class="review">
                            <div class="reviews_images">
                                <img src="img/user0_0.jpg" alt="">
                            </div>
                            <div class="reviews_descriptions">
                                <p>Подарили старшему сыну такие часики к Дню рождения. Он был очень рад подарку,
                                    преподнесли как
                                    часы шпиона, по которым можно связываться с штабом (родителями). Очень удобны,
                                    заряда
                                    хватает на 3 дня при активном использовании. Покупкой больше чем довольны, свои
                                    функции
                                    выполняют на твердую пятерку!</p>
                            </div>
                        </div>
                        <div class="review">
                            <div class="reviews_images">
                                <img src="img/user3_3.jpg" alt="">
                            </div>
                            <div class="reviews_descriptions">
                                <p>Моей дочери 4.5 года, я приобрела эти чудо часики, не пожалела не разу. Началось все
                                    с того,
                                    что доча выросла и захотела гулять во дворе с друзьями без меня, да и у меня не было
                                    особо
                                    времени сидеть во дворе на лавке. Мой поиск привел сюда, часы оказались еще и со
                                    встроенным
                                    телефоном!</p>
                            </div>
                        </div>
                        <div class="review">
                            <div class="reviews_images">
                                <img src="img/user10_10.jpg" alt="">
                            </div>
                            <div class="reviews_descriptions">
                                <p>Когда встал вопрос о покупке ребенку телефона, мы с женой остановили свой выбор на
                                    "Smart
                                    Kids Watch". Часы обладают такими же функциями, как и телефон. Только благодаря
                                    часам мы
                                    всегда в курсе, где находится наша малыш. Плюс они в разы дешевле телефона.
                                    Пользуемся уже 2
                                    года, пока все Окей.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> <!--todo тут работаем со слайдером отзывы-->

        <section id="reviewsSlider">
            <div class="container">
                <span class="leftSlideReview"></span>
                <span class="rightSlideReview"></span>
                <h2>Отзывы</h2>
                <div class="rewiewSliderContainer">
                    <div class="sliderActiveArea">
                        <div class="sliderContentRewiew">
                            <div class="sliderRewiewContent">
                                <div class="review">
                                    <div class="reviews_images">
                                        <img src="img/user8_8.jpg" alt="">
                                    </div>
                                    <div class="reviews_descriptions">
                                        <p>Выражаю огромное СПАСИБО за смарт-часы и отдельное СПАСИБО службе технической
                                            поддержки. Я как мама, захотела подключить несколько приложений отслеживания
                                            и как результат часы нигде не подключались(в инструкции было написано ОДИН
                                            раз регистрировать, но я не верила). Если бы не техподдержка то часы свою
                                            основную функцию не выполняли бы. Спасибо!!!!</p>
                                    </div>
                                </div>
                                <div class="review">
                                    <div class="reviews_images">
                                        <img src="img/user7_7.jpg" alt="">
                                    </div>
                                    <div class="reviews_descriptions">
                                        <p>Спасибо! Отличная компания, всё чётко-гладко, толково-доходчиво и самое
                                            главное доброжелательно и позитивно. Заказ (детские умные часы) привезли в
                                            оговорённое время, очень доброжелательно разъяснили-объяснили что к чему,
                                            всему обучили, оставили контактный телефон, помогли с настройками. Всё
                                            супер! Спасибо всей команде.</p>
                                    </div>
                                </div>
                                <div class="review">
                                    <div class="reviews_images">
                                        <img src="img/user9_9.jpg" alt="">
                                    </div>
                                    <div class="reviews_descriptions">
                                        <p>Все просто замечательно! Покупал смарт-часы своему сыну!) Качество самих
                                            часов просто на высоте! Связались со мной в течении минут 15! Менеджер
                                            Михаил,все чётко,вежливо и понятно мне разъяснил!) Привезли в течении двух
                                            часов !! Вообщем я очень и очень доволен! Ребята действительно ставят акцент
                                            на качество и обслуживание!! Еще обращусь к вам и не раз! Спасибо вам
                                            большое!!</p>
                                    </div>
                                </div>
                            </div>
                            <div class="sliderRewiewContent">
                                <div class="review">
                                    <div class="reviews_images">
                                        <img src="img/user6_6.jpg" alt="">
                                    </div>
                                    <div class="reviews_descriptions">
                                        <p>Отличная компания! Товар полностью соответствует указанному на сайте, как
                                            цена, так и характеристики. Доставка точно в срок, помощь при подключении.
                                            Дочка очень довольна. Рекомендую!</p>
                                    </div>
                                </div>
                                <div class="review">
                                    <div class="reviews_images">
                                        <img src="img/user5_5.jpg" alt="">
                                    </div>
                                    <div class="reviews_descriptions">
                                        <p>Магазин сработал отлично, товар был в наличии и по указанной цене, доставили
                                            на следующий день после заказа по Беларуси.</p>
                                    </div>
                                </div>
                                <div class="review">
                                    <div class="reviews_images">
                                        <img src="img/user4_4.jpg" alt="">
                                    </div>
                                    <div class="reviews_descriptions">
                                        <p>Покупали вместе с женой ребёнку в подарок умные часы. Ребята рассказали
                                            человеческим языком о функциях модели и быстро подобрали цвет. Качеством
                                            товара и сервисом остались очень довольны. Спасибо.</p>
                                    </div>


                                </div>
                            </div>
                            <div class="sliderRewiewContent">
                                <div class="review">
                                    <div class="reviews_images">
                                        <img src="img/user0_0.jpg" alt="">
                                    </div>
                                    <div class="reviews_descriptions">
                                        <p>Подарили старшему сыну такие часики к Дню рождения. Он был очень рад подарку,
                                            преподнесли как часы шпиона, по которым можно связываться с штабом
                                            (родителями). Очень удобны, заряда хватает на 3 дня при активном
                                            использовании. Покупкой больше чем довольны, свои функции выполняют на
                                            твердую пятерку!</p>
                                    </div>
                                </div>
                                <div class="review">
                                    <div class="reviews_images">
                                        <img src="img/user3_3.jpg" alt="">
                                    </div>
                                    <div class="reviews_descriptions">
                                        <p>Моей дочери 4.5 года, я приобрела эти чудо часики, не пожалела не разу.
                                            Началось все с того, что доча выросла и захотела гулять во дворе с друзьями
                                            без меня, да и у меня не было особо времени сидеть во дворе на лавке. Мой
                                            поиск привел сюда, часы оказались еще и со встроенным телефоном!</p>
                                    </div>
                                </div>
                                <div class="review">
                                    <div class="reviews_images">
                                        <img src="img/user10_10.jpg" alt="">
                                    </div>
                                    <div class="reviews_descriptions">
                                        <p>Когда встал вопрос о покупке ребенку телефона, мы с женой остановили свой
                                            выбор на "Smart Kids Watch". Часы обладают такими же функциями, как и
                                            телефон. Только благодаря часам мы всегда в курсе, где находится наша малыш.
                                            Плюс они в разы дешевле телефона. Пользуемся уже 2 года, пока все Окей.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </section>

        <section class="achievements">
            <div class="container">
                <h2>Наши достижения</h2>
                <div class="our_achievements">
                    <div class="years_on_the_market">
                        <p class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="-3 -3 23 22"
                                 id="icon">
                                <path d="M13 15h-1.5v-4.5-.004c-.003-.025-.01-.05-.017-.073a.488.488 0 0 0-.44-.415l-2.794-2.01
     2.792-2.007c.23-.02.406-.19.44-.414.007-.024.015-.048.017-.073V1H13a.5.5 0 0 0 0-1H2a.5.5 0 0 0
      0 1h1.5v4.503c0 .025.008.05.014.074a.49.49 0 0 0 .443.414L6.747 8l-2.79 2.008a.49.49 0
       0 0-.442.415c-.006.024-.013.048-.015.073V15H2a.5.5 0 0 0 0 1h11a.5.5 0 0 0 0-1zM4.5 5.3V1h6v4.3l-3 2.16-3-2.16zm6
        9.7h-6v-4.302l3-2.158 3 2.158v4.3z"></path>
                            </svg>
                        </p>
                        <p class="achievements_count">2</p>
                        <p>Года на рынке Smart Watch.</p>
                    </div>
                    <div class="orders_per_day">
                        <p class="icon">

                            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="-3 -3 22 22"
                                 id="icon">
                                <path d="M16 2a2 2 0 0 0-2-2H2a2 2 0 0 0-2 2V3c0 .173.028.338.07.5C.027 3.66 0 3.825 0 4v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V4c0-.174-.03-.34-.07-.5.04-.16.07-.325.07-.498V2zm-1 12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V4.722c.294.172.633.278 1 .278h12a1.98 1.98 0 0 0 1-.277V14zm0-11a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v1zm-1.5 4h-2a.5.5 0 0 0 0 1h.5a4 4 0 0 1-8 0h.5a.5.5 0 0 0 0-1h-2a.5.5 0 0 0 0 1H3c0 2.762 2.237 5 5 5s5-2.238 5-5h.5a.5.5 0 0 0 0-1z"></path>
                            </svg>
                        </p>
                        <p class="achievements_count">25</p>
                        <p>Заказов в день мы обрабатываем и доставляем нашим довольным покупателям.</p>
                    </div>
                    <div class="feedback">
                        <p>
                            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="-3 -3 22 22"
                                 id="icon">
                                <path d="M11.927 11.46a6.003 6.003 0 0 0-2.225-1.536A4.5 4.5 0 1 0 3 6a4.5 4.5 0 0 0 2.298 3.923A6 6 0 0 0 1.5 15.5v1h1v-1a5 5 0 0 1 5-5c1.492 0 2.816.668 3.732 1.703.198-.28.435-.52.695-.742zM4 6a3.5 3.5 0 0 1 7 0 3.5 3.5 0 0 1-7 0zm12.357 6.61a.485.485 0 0 0-.684 0l-2.695 2.7-.684-.684a.482.482 0 0 0-.683.684l.935.936c.02.04.04.078.072.11.1.1.23.142.36.138.13.004.26-.04.36-.14.03-.03.05-.07.07-.108l2.948-2.95c.19-.19.19-.497.002-.687z"></path>
                            </svg>
                        </p>
                        <p class="achievements_count">371</p>
                        <p>Положительный отзыв.</p>
                    </div>
                </div>
            </div>
        </section>

        <section id="sertification">

            <div class="container">
                <h2>Документы</h2>

                <div class="sertificationContainer">

                    <div class="sertificateBlock">
                        <img src="img/sertificateOne.jpg" alt="Sertificate_1"
                             data-image-full-screen="img/sertificateOne.jpg">
                    </div>

                    <div class="sertificateBlock">
                        <img src="img/sertificateTwo.jpg" alt="Sertificate_2"
                             data-image-full-screen="img/sertificateTwo.jpg">
                    </div>

                    <div class="sertificateBlock">
                        <img src="img/sertificateThree.jpg" alt="Sertificate_3"
                             data-image-full-screen="img/sertificateThree.jpg">
                    </div>

                </div>
            </div>


        </section>

        <section class="feedback_bg">
            <div class="container">
                <div class="feedback_container">
                    <h2><span>Остались вопросы?</span> <span>Оставьте заявку.</span></h2>
                    <span class="button feedback_button"
                          onclick="yaCounter50378515.reachGoal('targetButton4'); return true;">Оставить заявку</span>
                </div>
            </div>
        </section>

        <section>
            <div class="feedback_hidden_form"></div>
        </section>

        <section id="hidden_block">
            <div class="feedback_hidden_block"></div>


        </section>

        <div class="form_container">
            <div class="feedback_hidden_image"></div>
            <div class="feedback_hidden_form">
                <form action="" method="POST"
                      onsubmit="yaCounter50378515.reachGoal('targetButtonFinishSend'); return true;"><!--mail.php-->
                    <span class="close_hidden_block"></span>
                    <h3>Заказать обратный звонок</h3>
                    <label for="name">Введите ваше имя*
                        <input type="text" name="name" id="name" required>
                    </label>
                    <label for="phone">Введите ваш телефон*
                        <input type="tel" name="phone" id="phone" required>
                    </label>
                    <button class="button send"
                            onclick="yaCounter50378515.reachGoal('targetButtonFinish'); return true;">Заказать Smart
                        Kids Watch
                    </button>
                    <span class="inform_f">Мы не передаем Вашу персональную информацию третьим лицам</span>
                </form>
            </div>
        </div>

        <span class="slide_back"></span>

        <span class="showMessage"><span class="close_hidden_block"></span></span>


        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/validation.js"></script>
        <script src="js/script.js?v=0003"></script>

        </body>
    </div>


    </html>

