<?php
require ('adminData.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$data = $_POST;
$salt = 'BestSmartWatch2018';


if(!isset($data['submit']) || empty($data['submit']) || $data['submit'] !== 'send' ){
    header('location: index.php');
    exit();
}

if(!isset($data['login']) || !isset($data['password']) || empty($data['login']) || empty($data['password'])){
    header('location: index.php');
    exit();
}

if($data['login'] === $adminLogin && $data['password'] === $adminPassword) {
    session_start();
    $_SESSION['al'] = 1;
    header('location: /index.php');

}else {
    header('location: index.php');
    exit();
}
?>