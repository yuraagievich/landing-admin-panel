<?
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>

    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/styleAdmin.css?v=0002">

        <title>Authentication</title>
    </head>
    <body>
    <div class="centering">
        <form class="form-1" action="auth.php" method="POST">
            <p class="field">
                <input type="text" name="login" placeholder="Логин">
                <i class="icon-user icon-large"></i>
            </p>
            <p class="field">
                <input type="password" name="password" placeholder="Пароль">
                <i class="icon-lock icon-large"></i>
            </p>
            <p class="submit">
               <!-- <button type="submit" name="submit"><i class="icon-arrow-right icon-large"></i></button>-->
                <button type="submit" name="submit" value="send">
                    <i class="icon-user icon-large"></i>
                </button>
            </p>
        </form>
    </div>
    </body>

<?php

?>