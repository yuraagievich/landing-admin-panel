'use strict';



let sendObj = {};

function createNewTinyMseArea(selector){
    tinymce.init({
        selector: selector,  // change this value according to your HTML
        plugin: 'a_tinymce_plugin',
        a_plugin_option: true,
        a_configuration_option: 400,
        inline: true,
        plugins: [
            'advlist',
            'autolink',
            'link',
            'image',
            'lists',
            'charmap',
            'searchreplace',
            'wordcount',
            'visualblocks',
            'visualchars',
            'code',
            'fullscreen',
            'insertdatetime',
            'media nonbreaking',
            'save',
            'table',
            'contextmenu',
            'directionality',
            'emoticons',
            'template',
            'paste',
            'textcolor',
            'colorpicker'
        ],
    });
}

function createPopUpSaveOrCancelChange(context, where, previous, real) {
    let popUpContainer = $('<div>', {'class': 'popUpContainer'});
    $(where).append(popUpContainer);

    let hiddenSpan = $('<span>', {'class': 'dataField', 'data-field': context});
    popUpContainer.append(hiddenSpan);

    let popUpTitle = $('<h4>', {'class': 'popUpTitle', 'text': 'Сохранить изменениея?'});
    popUpContainer.append(popUpTitle);

    let popUpPrevious = $('<p>', {'class': 'popUpPrevious', 'text': 'Было: '});
    popUpContainer.append(popUpPrevious);

    let popUpPrevSpan = $('<span>', {'class': 'popUpPrev', 'text': previous});
    popUpPrevious.append(popUpPrevSpan);

    let popUpReal = $('<p>', {'class': 'popUpReal', 'text': 'Стало: '});
    popUpContainer.append(popUpReal);

    let popUpRealSpan = $('<span>', {'class': 'popUpRealVal', 'text': real});
    popUpReal.append(popUpRealSpan);

    let buttonContainer = $('<div>', {'class': 'buttonContainer'});
    popUpContainer.append(buttonContainer);

    let buttonApply = $('<span>', {'class': 'apply', 'text': 'Сохранить'});
    buttonContainer.append(buttonApply);

    let buttonCancel = $('<span>', {'class': 'cancel', 'text': 'Отмена'});
    buttonContainer.append(buttonCancel);

    let closePopUp = $('<span>', {'class': 'close'});
    popUpContainer.append(closePopUp);

}

function getArrayOfChangeArea() {
    let tempObj = [];
    let returnStr = '';
    let allTagsBody = $('.changeArea');

    allTagsBody.each(function (i, elem) {
        let elemClasses = $(elem).attr('class').replace('changeArea ', '.');
        tempObj.push(elemClasses);
    });
    returnStr = tempObj.join(',');
    return returnStr;
   }

$(document).ready(function () {

    let arrayOfChangeableAreas = getArrayOfChangeArea();

    console.log(`arrayOfChangeableAreas`);
    console.log(arrayOfChangeableAreas);

    createNewTinyMseArea(arrayOfChangeableAreas);

    let previous = '';

    $(document).on('click', '.phoneOne, .reviews, .about, .pay, .descr', function () {
        previous = $(this).text();
    });

    $(document).on('blur', '.phoneOne, .reviews, .about, .pay, .descr', function () {
        let context = $(this).attr('class').split(' ')[0];
        let real = $(this).text();
        createPopUpSaveOrCancelChange(context,'body', previous, real);
    });


    $(document).on('click', '.apply', function () {

        let property  = $('.dataField').attr('data-field');

        if(typeof sendObj[property] === 'undefined') {
            sendObj[property]  = $('.popUpRealVal').text();
        }else {
            delete sendObj[property];
            sendObj[property] = $('.popUpRealVal').text();
        }


        console.log(sendObj);

        $('.popUpContainer').remove();

    });

    $(document).on('click', '.close, .cancel', function () {
        let context = $('.dataField').attr('data-field');
        $('.' + context).text($('.popUpPrev').text());
        $('.popUpContainer').remove();
        console.log(sendObj);
    });


    $(document).on('click', '.relog', function (e) {
        e.preventDefault();
        $.ajax({
            method: 'POST',
            url: "admin/relog.php",
            data: sendObj,
            success: function (data) {
                console.log(data);
                if(data === 'reload') {
                    window.location.reload()
                }
            },
            error: function () {
                console.error(`ERROR get-all`);
            }
        });
    })


});





